<?php

namespace BlogBundle\Entity\Translation;

use AppBundle\Entity\Translation\TranslationTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryTranslation
 *
 * @ORM\Entity()
 * @ORM\Table(name="category_translation")
 */
class CategoryTranslation
{
    use TranslationTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CategoryTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
